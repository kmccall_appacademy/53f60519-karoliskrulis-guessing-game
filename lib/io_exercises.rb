# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game

  r = [*1..100].shuffle.last
  attempts = 1

  while true

    puts "Guess a number"
    input = Integer(gets.chomp)
    attempts += 1

    if r == input
      puts "#{input} is correct!"
      break
    elsif puts input > r ? "#{input} is too high." : "#{input} is too low"
    end

    puts "It took #{attempts} number of guesses."
  end

end
